package dk.zlepper.superloader

import java.util.*

class LookupResult<T>(
        private val service: Class<T>
) {
    private val listeners = HashSet<LookupListener<T>>()
    private val implementations = HashSet<T>()

    fun addListener(listener: LookupListener<T>) {
        listeners += listener
    }

    fun removeListener(listener: LookupListener<T>) {
        listeners -= listener
    }

    val instances: Set<T>
        get() {
            return implementations.toSet()
        }

    /**
     * Adds a service loader, and emits change events
     */
    fun addClassLoader(loader: ClassLoader) {
        val services = ServiceLoader.load(service, loader)
        implementations += services
        listeners.forEach { listener ->
            services.forEach {
                listener.serviceLoaded(it)
            }
        }
    }

    /**
     * Removes a service loader and emits change events
     */
    fun removeClassLoader(loader: ClassLoader) {
        val services = ServiceLoader.load(service, loader)
        implementations -= services
        listeners.forEach { listener ->
            services.forEach {
                listener.serviceUnloaded(it)
            }
        }
    }
}