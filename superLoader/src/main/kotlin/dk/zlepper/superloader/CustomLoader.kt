package dk.zlepper.superloader

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.io.File
import java.net.URLClassLoader
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchService
import java.util.*

/**
 * Handles loading and unloading modules at runtime
 */
object CustomLoader {

    /**
     * The class loaders we currently have active
     */
    private val loaders = HashMap<String, Pair<File, URLClassLoader>>()
    /**
     * All the lookupResults in the application
     */
    private val lookups = Collections.newSetFromMap(WeakHashMap<LookupResult<*>, Boolean>())
    /**
     * Jobs for watching for file changed
     */
    private val watchJobs = HashMap<String, Job>()

    /**
     * Starts watching a directory for changes, and loads any modules in there
     */
    fun addDirectoryToWatch(dir: File) {
        // Handle watching for changes
        val dirPath = dir.toPath()
        val watchService = dirPath.fileSystem.newWatchService()

        dirPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY)

        watchDirectory(watchService, dir)

        // Handle whatever is there now
        val jars = dir.listFiles()

        for (jar in jars) {
            if (jar.extension == "jar") {
                addJar(jar)
            }
        }
    }

    /**
     * Removes a directly from the module loader
     */
    fun removeDirectoryFromWatch(dir: File) {
        val path = dir.absolutePath.replace("\\", "/")
        watchJobs.remove(path)?.cancel()
        dir.listFiles().forEach {
            if (it.extension == "jar") {
                removeJar(it)
            }
        }
    }

    /**
     * Handles the watching of the directory
     */
    private fun watchDirectory(watchService: WatchService, dir: File) {
        val job = launch {

            val key = watchService.take()

            while (isActive) {

                val events = key.pollEvents()
                        .map { it.context().toString() }
                        .map { "${dir.absolutePath}/$it" }
                        .map { it.replace("\\", "/") }
                        .filter { it.endsWith(".jar") }
                        .toSet()

                events.forEach {
                    handleWatchEvent(it)
                }

                delay(1000)
            }
        }

        watchJobs[dir.absolutePath.replace("\\", "/")] = job
    }

    /**
     * Handle figuring out what each watch event is supposed to mean.
     * This normalizes events, as java and windows makes no sense what so ever.
     */
    private fun handleWatchEvent(filename: String) {
        // Figure out the type of event we are dealing with
        // And if we should care about it
        val file = File(filename)
        val exists = file.exists()
        val registered = loaders.containsKey(filename)
        when {
        // The file exists, and we already have it registered: It was changed
            exists && registered -> updateJar(file)

        // The file exists, and we don't have it registered: It was created
            exists && !registered -> addJar(file)

        // The file doesn't exist, and we have it registered: It was deleted
            !exists && registered -> removeJar(file)

        // The file doesn't exist, and we don't have it registered: No clue, but we don't care
            else -> {
                println("Somebody was a fast bastard huh? '$filename'\n${loaders.keys}")
            }
        }

    }

    /**
     * Handles updating a jar file
     */
    private fun updateJar(jar: File) {
        if (loaders.containsKey(jar.absolutePath)) {
            removeJar(jar)
        }
        addJar(jar)
    }

    /**
     * Handles adding a new jar file
     */
    private fun addJar(jar: File) {
        val path = jar.absolutePath.replace("\\", "/")
        println("Adding jar: $path")
        val tempFile = File.createTempFile(jar.name, jar.extension)
        jar.copyTo(tempFile, overwrite = true)

        val url = tempFile.toURI().toURL()
        val loader = URLClassLoader(arrayOf(url), ClassLoader.getSystemClassLoader())

        lookups.forEach { it.addClassLoader(loader) }

        loaders[path] = Pair(tempFile, loader)
    }

    /**
     * Handles removing or deleting a jar file
     */
    private fun removeJar(jar: File) {
        val path = jar.absolutePath.replace("\\", "/")
        // Get the temp file and class loader for the jar, if it exists, otherwise don't do anything
        val p = loaders[path] ?: return

        val (tempFile, loader) = p

        lookups.forEach { it.removeClassLoader(loader) }
        // Make the loader free up the file
        loader.close()
        // Delete the temp file
        tempFile.delete()

        loaders.remove(path)
    }

    /**
     * Looks up the given classes in the loaders
     */
    fun <T> lookupResult(service: Class<T>): LookupResult<T> {
        val result = LookupResult(service)

        loaders.values.forEach {
            result.addClassLoader(it.second)
        }

        lookups += result

        return result
    }
}

