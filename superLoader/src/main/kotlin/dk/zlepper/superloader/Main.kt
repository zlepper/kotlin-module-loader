package dk.zlepper.superloader

import dk.zlepper.interfaces.AweasomeService
import java.io.File

class listener : LookupListener<AweasomeService> {
    override fun serviceLoaded(service: AweasomeService) {
        println("Loaded service ${service}")
        service.doStuff()
    }

    override fun serviceUnloaded(service: AweasomeService) {
        println("Unloaded service ${service}")
        service.doStuff()
    }

}

fun main(args: Array<String>) {
    CustomLoader.addDirectoryToWatch(File("C:\\dev\\school\\kotlin-module-loader\\testlibs"))
    val result = CustomLoader.lookupResult(AweasomeService::class.java)

    result.addListener(listener())
    println(result.instances)

    Thread.sleep(100000)
}