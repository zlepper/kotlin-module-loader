package dk.zlepper.superloader

interface LookupListener<T> {
    fun serviceLoaded(service: (T))
    fun serviceUnloaded(service: (T))
}