package dk.zlepper.submodule1

import dk.zlepper.interfaces.AweasomeService

class SuperDuperService : AweasomeService {
    override fun doStuff() {
        println("Version 2")
    }
}